# API 360 endpoints to handle 360 platform in China


- [Login](#login-)
- [Logout](#logout-)
- [Get User Info](#get-user-info-)
- [Set Avatar](#set-avatar-)
- [Get Device List](#get-device-list-)
- [Get Device Step Info](#get-device-step-info-)


#### Login ✅
 
  User login the system. `Encrypt With Before Login Method`.
  
  All parameters are required because we require customer's system language and time zone.
  Token and secret are return by login, and they're use for request APIs after login.
 
  - POST: /v2/login
  
  - Parameters encrypted in p:
  
     param        | meaning              | note                              
     ------------ | -------------------- | --------------------------------- 
     country_code |                      |                                   
     phone_number |                      |                                   
     password     | user password        |                                   
     user_lang    | user system language | like zh_CN, es_ES                 
     time_zone    | user system timezone | like Asia/Shanghai, Europe/Madrid 

  - Body: No need
  
  - Response
  
     field     | meaning                  | note          
     --------- | ------------------------ | ------------- 
     secret    | user secret for encrypt  | 32 characters 
     token     | user identify in request | 32 characters 
     user_info | user info in system      | JsonObject    
  
     - fields in user_info
          
          field        | meaning        | note                         
          ------------ | -------------- | ---------------------------- 
          qid          | user id        | 10 digits for now            
          nick_name    | user nick name | phone number default for now 
          icon_url     | user icon url  |                              
          email        | user email     |                              
          country_code |                |                              
          phone_number |                |                              
          user_lang    |                |                              
          time_zone    |                |                              
 
 


#### Logout ✅
  
   User logout system
  
   - POST: /v2/account/logout
   
   - Parameters: Don't need any other parameters.

   - Body: No need
  
   - Response: 
     
     field     | meaning                  | note          
     --------- | ------------------------ | ------------------------ 
     success   | logout success or not    | 1 for success, 0 for not 
   

#### Get User Info ✅
  
  Get user info
  
  - GET: /v2/account/info
  
  - Parameters
  
     don't need any other parameters
  
  - Response
  
     just like `Login` but only user_info
     
     


#### Set Avatar ✅

   Set avatar for account. 400x400 and JPG format would be nice.
   
   - PUT: /v2/account/avatar
   
   - Parameters encrypted in p:
   
      param       | meaning                                     | note                    
      ----------- | ------------------------------------------- | ----------------------- 
      hash        | md5 hash of original avatar image data      | encrypted in p in Query 
   
   - Body
    
     AES encrypted and base64 encoded image data  
   
   - Response: Same with `Get user info` and user_info in `Login`



     
#### Get Device List ✅

   Keep device_id to get device list, or get specify device info.
   
   - GET: /v2/device
   
   - Parameters encrypted in p
 
      param     | meaning                                          | note 
      --------- | ------------------------------------------------ | ---- 
      device_id | specify a device, keep empty to get device list  |   
   
   - Response
   
      field       | meaning               | note
      ----------- | --------------------- | ------------------
      devices     | device list           | device JsonObject
      
      - device
      
         field             | meaning                    | note
         ----------------- | -------------------------- | ---------
         device_id         | device id                  |
         name              | child's name               |
         device_type       | device type                | 
         device_type_name  | device type display name   | 
         qr                | qr code of watch           |
         imei              | IMEI                       |
         ver               | device rom version         |
         country_code      | country code               |
         phone_number      | phone number               |
         device_timezone   | device time zone           | 
         active_time       | device active time         | 
         cor_name          | name display on device     | 
         is_admin          | Device's admin             | 1: yes 0: no
         icon_url          | child's profile image      | is 1 for default avatar, or an image url  
         location          | child's profile image      | See api/locate.md `Get Watch's Location`
         step_info         | step info                  | See api/settings.md `Get Device Step Info`
      

#### Get Step ✅

   Keep device_id to get steps
   
   - GET: /v2/device/step
   
   - Parameters encrypted in p
 
      param     | meaning                                          | note 
      --------- | ------------------------------------------------ | ---- 
      device_id | specify a device, keep empty to get device list  |   
   
   - Response
   
      field       | meaning               | note
      ----------- | --------------------- | ------------------
      devices     | device list           | device JsonObject

#### Get Device Step Info ✅

   Get device's step info of a day. The date must be within 31 days.

   - GET: /v2/device/stepInfo
   
   - Parameters
   
      param        | meaning          | note 
      ------------ | ---------------- | ------------------------------------
      device_id    |                  |      
      date         | specific date    | default is today. format `YYYY-mm-dd`     
   
   - Response:
   
      field       | meaning               | note
      ----------- | --------------------- | ------------------
      step_info   |                       | step info JsonObject
  
      - step_info
      
         field             | meaning                    | note
         ----------------- | -------------------------- | ---------
         distance          | distance of this day       | int, in meters
         step              | step count of this day     | int
         target_step       | target step of this day    | int
         calorie           | calorie of this day        | int




